/**
 * This handles compiling a component to Javascript.
 * Will compile into a JS module and will function similarly to
 * Web Components. See documentation here
 * https://svelte.dev/docs#Custom_element_API
 */
const fs = require("fs");
const svelte = require("svelte/compiler")
const yargs = require("yargs");
const {hideBin} = require('yargs/helpers')

const args = yargs(hideBin(process.argv)).argv;

// check to see if component exists. Assumes no file extension exists.
let path = `./src/components/${args.component}.svelte`

if(fs.existsSync(path)){
    console.log("Compiling file")

    const contents = fs.readFileSync(path,"utf8");

    const result = svelte.compile(contents,{
        customElement:true,
        format: args.format !== undefined ? args.format : "esm",
        name: args.name !== undefined ? args.name : "Component",
        tag: args.tag !== undefined ? args.tag : "my-component", // TODO not sure if this will override options set in component,
        css: args.css !== undefined ? args.css : true
    });

    if(args.output){
        fs.writeFileSync(`${args.output}/${args.component}.js`,result.js.code);
    }else{
        fs.writeFileSync(`${process.cwd()}/${args.component}.js`,result.js.code);
    }
}else{
    throw new Error("Unable to find component source at " + path);
}
