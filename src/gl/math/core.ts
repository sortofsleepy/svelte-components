import mat4 from "./mat4";
/**
 * Flattens an nested array that is assumed to be nested with child arrays used in place of
 * an actual vector object. Note, this does not check for completeness and will automatically
 * only take the first 3 values of the child arrays
 * @param array the parent array
 * @param reSize {Number} the size of each nested array. Determines  how the array is unrolled.
 * @returns {Array}
 */
export function flattenArray(array:Array<any>,reSize:number=3){

    let fin = [];
    let len = array.length;
    for(let i = 0; i < len; ++i){
        let arr = array[i];

        if(reSize === 4){
            fin.push(arr[0],arr[1],arr[2],arr[3]);
        }else if(reSize === 3){
            fin.push(arr[0],arr[1],arr[2]);
        }else{
            fin.push(arr[0],arr[1]);
        }
    }

    // map through to ensure we don't have any unknown / undefined values.
    // if so, set to 0
    fin = fin.map(itm => {
        if(itm === undefined || itm === null){
            return 0.0;
        }else{
            return itm;
        }

    })

    return fin;
}

/**
 * Does subtraction between two arrays. Assumes both arrays have 3 values each inside
 * @param array1 {Array} the array to subtract from
 * @param array2 {Array} the array to subtract
 * @returns {*[]}
 */
export function subArrays(array1:Array<number>,array2:Array<number>){

    let x1 = array1[0];
    let y1 = array1[1];
    let z1 = array1[2];

    let x2 = array2[0];
    let y2 = array2[1];
    let z2 = array2[2];
    return [x1 - x2, y1 - y2, z1 - z2];
}

/**
 * Adds 2 arrays together
 * @param array1 {Array} array 1
 * @param array2 {Array} array 2
 */
export function addArrays(array1:Array<number>,array2:Array<number>){

    let x1 = array1[0];
    let y1 = array1[1];
    let z1 = array1[2];

    let x2 = array2[0];
    let y2 = array2[1];
    let z2 = array2[2];
    return [x1 + x2, y1 + y2, z1 + z2];
}

/**
 * Multiplies all elements in an array by a scalar value
 * @param array1 {Array} an array to multiply against
 * @param scalar {number} the scalar value.
 */
export function multiplyScalar(array1:Array<number>,scalar:number){

    let x1 = array1[0];
    let y1 = array1[1];
    let z1 = array1[2];

    return [x1 * scalar, y1 * scalar, z1 * scalar];
}


/**
 * Converts value to radians
 * @param deg{number} a value in degrees
 */
export function toRadians(deg:number){
    return (deg * Math.PI) / 180;
}


/**
 * Normalizes the values in an array
 * @param vec {Array} the array to  normalize.
 */
export function normalizeArray(vec:Array<number>){
    var mag = 0
    for (var n = 0; n < vec.length; n++) {
        mag += vec[n] * vec[n]
    }
    mag = Math.sqrt(mag)

    // avoid dividing by zero
    if (mag === 0) {
        return Array.apply(null, new Array(vec.length)).map(Number.prototype.valueOf, 0)
    }

    for (var n = 0; n < vec.length; n++) {
        vec[n] /= mag
    }

    return vec
}

/**
 * Cross function.
 * @param a first "vector" / array
 * @param b second "vector" / array
 * @returns {[*,*,*]}
 */
export function cross(a,b){
    if(a.length === 3 && b.length === 3){
        let a1 = a[0];
        let a2 = a[1];
        let a3 = a[2];
        let b1 = b[0];
        let b2 = b[1];
        let b3 = b[2];

        return [a2 * b3 - a3 * b2, a3 * b1 - a1 * b3, a1 * b2 - a2 * b1]
    }
}

/**
 * Creates an array with a range of values
 * @param from {Number} the value to start from
 * @param to {Number} the value end at.
 * @returns {Array}
 */
export function range(from,to){
    var result = [];
    var n = from;
    while (n < to) {
        result.push(n);
        n += 1;
    }
    return result;
}

/**
 * Returns a random vec3(in the form of an array)
 * @returns {*[]}
 */
export function randVec3(offset=1){
    return[
        Math.random() * offset,
        Math.random() * offset,
        Math.random() * offset
    ]
}
export function randVec4(offset=1){
    return[
        Math.random() * offset,
        Math.random() * offset,
        Math.random() * offset,
        Math.random() * offset
    ]
}
/**
 * Performs linear interpolation of a value
 * @param value the value to interpolate
 * @param min the minimum value
 * @param max the maximum value
 * @returns {number}
 */
export function lerp(value:number,min:number=0,max:number=1){
    return  min + (max -min) * value;
}

/**
 * Smoothly ease into values.
 * Taken from THREE.Math
 * @param x {Number} value to modify
 * @param min {Number}
 * @param max {Number}
 * @returns {number}
 */
export function smoothStep(x:number,min:number,max:number){
    if(x <= min) return 0;
    if(x >= max) return 1;
    x = (x - min) / (max - min);
    return x * x * (3 - 2 * x);
}

/**
 * Returns a random float value between two numbers
 * @param min {Number} the minimum value
 * @param max {Number} the maximum value
 * @returns {number}
 */
export function randFloat(min:number=0,max:number=1){
    return min + Math.random() * (max - min + 1);
}

/**
 * Returns a random integer value between two numbers
 * @param min {Number} the minimum value
 * @param max {Number} the maximum value
 * @returns {number}
 */
export function randInt(min:number=0,max:number=1){
    return Math.floor(min + Math.random() * (max - min + 1));
}

/**
 * Very simple array cloning util.
 * Note - only works with arrays who have 3 elements
 * @param arrayToClone the array to clone
 * @returns {*[]} the new array
 */
export function cloneArray(arrayToClone:[number,number,number]){
    return [
        arrayToClone[0],
        arrayToClone[1],
        arrayToClone[2]
    ]
}

/**
 * Ensures a value lies in between a min and a max
 * @param value
 * @param min
 * @param max
 * @returns {*}
 */
export function clamp(value:number,min:number,max:number){
    return min < max
        ? (value < min ? min : value > max ? max : value)
        : (value < max ? max : value > min ? min : value)
}


/**
 * Returns the magnitude of a 3 component vector
 * @param v {Array}
 */
export function magnitudeOfVector(v:[number,number,number]){
    return Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
}

/**
 * Normalizes a 3 component vector.
 * @param out {Array} where values get assigned to
 * @param v {Array} the array to normalize.
 */
export function normalizeVector(out:[number,number,number],v:[number,number,number]){
    let inverseMagnitude = 1.0 / magnitudeOfVector(v);
    out[0] = v[0] * inverseMagnitude;
    out[1] = v[1] * inverseMagnitude;
    out[2] = v[2] * inverseMagnitude;
    return out;
}

export function normalizeColor(vector:Array<number>){
    if(vector.length > 4){
        return ;
    }

    vector[0] = vector[0] / 255.0;
    vector[1] = vector[1] / 255.0;
    vector[2] = vector[2] / 255.0;

    if(vector.length === 4){
        vector[3] = vector[3] / 255.0;
    }

    return vector;
}



// based off of David Li's work
// https://github.com/dli/fluid/blob/ac3ee551ee33caaf4c0aa38da21e2be5562fd5ab/boxeditor.js#L879
export function getMouseRay(
    mouse:any,
    projectionMatrix:Array<number>,
    viewMatrix:Array<number>,
    canvas:any){

    let invert = mat4.scalar.invert;

    let fov = 2.0 * Math.atan(1.0 / projectionMatrix[5]);

    let viewSpaceMouseRay = [
        this.mouseX * Math.tan(fov / 2.0) * (canvas.width / canvas.height),
        this.mouseY * Math.tan(fov / 2.0),
        -1.0];

    let inverseViewMatrix = invert([], viewMatrix);
    let mouseRay = transformDirectionByMatrix([], viewSpaceMouseRay, inverseViewMatrix);
    normalizeVector(mouseRay, mouseRay);


    let rayOrigin = this.camera.getPosition();

    return {
        origin: rayOrigin,
        direction: mouseRay
    };

}


export function transformDirectionByMatrix(out, v, m) {
    var x = v[0], y = v[1], z = v[2];
    out[0] = m[0] * x + m[4] * y + m[8] * z;
    out[1] = m[1] * x + m[5] * y + m[9] * z;
    out[2] = m[2] * x + m[6] * y + m[10] * z;
    out[3] = m[3] * x + m[7] * y + m[11] * z;
    return out;
}

export function invertMatrix(out, m) {
    var m0 = m[0], m4 = m[4], m8 = m[8], m12 = m[12],
        m1 = m[1], m5 = m[5], m9 = m[9], m13 = m[13],
        m2 = m[2], m6 = m[6], m10 = m[10], m14 = m[14],
        m3 = m[3], m7 = m[7], m11 = m[11], m15 = m[15],

        temp0 = m10 * m15,
        temp1 = m14 * m11,
        temp2 = m6 * m15,
        temp3 = m14 * m7,
        temp4 = m6 * m11,
        temp5 = m10 * m7,
        temp6 = m2 * m15,
        temp7 = m14 * m3,
        temp8 = m2 * m11,
        temp9 = m10 * m3,
        temp10 = m2 * m7,
        temp11 = m6 * m3,
        temp12 = m8 * m13,
        temp13 = m12 * m9,
        temp14 = m4 * m13,
        temp15 = m12 * m5,
        temp16 = m4 * m9,
        temp17 = m8 * m5,
        temp18 = m0 * m13,
        temp19 = m12 * m1,
        temp20 = m0 * m9,
        temp21 = m8 * m1,
        temp22 = m0 * m5,
        temp23 = m4 * m1,

        t0 = (temp0 * m5 + temp3 * m9 + temp4 * m13) - (temp1 * m5 + temp2 * m9 + temp5 * m13),
        t1 = (temp1 * m1 + temp6 * m9 + temp9 * m13) - (temp0 * m1 + temp7 * m9 + temp8 * m13),
        t2 = (temp2 * m1 + temp7 * m5 + temp10 * m13) - (temp3 * m1 + temp6 * m5 + temp11 * m13),
        t3 = (temp5 * m1 + temp8 * m5 + temp11 * m9) - (temp4 * m1 + temp9 * m5 + temp10 * m9),

        d = 1.0 / (m0 * t0 + m4 * t1 + m8 * t2 + m12 * t3);

    out[0] = d * t0;
    out[1] = d * t1;
    out[2] = d * t2;
    out[3] = d * t3;
    out[4] = d * ((temp1 * m4 + temp2 * m8 + temp5 * m12) - (temp0 * m4 + temp3 * m8 + temp4 * m12));
    out[5] = d * ((temp0 * m0 + temp7 * m8 + temp8 * m12) - (temp1 * m0 + temp6 * m8 + temp9 * m12));
    out[6] = d * ((temp3 * m0 + temp6 * m4 + temp11 * m12) - (temp2 * m0 + temp7 * m4 + temp10 * m12));
    out[7] = d * ((temp4 * m0 + temp9 * m4 + temp10 * m8) - (temp5 * m0 + temp8 * m4 + temp11 * m8));
    out[8] = d * ((temp12 * m7 + temp15 * m11 + temp16 * m15) - (temp13 * m7 + temp14 * m11 + temp17 * m15));
    out[9] = d * ((temp13 * m3 + temp18 * m11 + temp21 * m15) - (temp12 * m3 + temp19 * m11 + temp20 * m15));
    out[10] = d * ((temp14 * m3 + temp19 * m7 + temp22 * m15) - (temp15 * m3 + temp18 * m7 + temp23 * m15));
    out[11] = d * ((temp17 * m3 + temp20 * m7 + temp23 * m11) - (temp16 * m3 + temp21 * m7 + temp22 * m11));
    out[12] = d * ((temp14 * m10 + temp17 * m14 + temp13 * m6) - (temp16 * m14 + temp12 * m6 + temp15 * m10));
    out[13] = d * ((temp20 * m14 + temp12 * m2 + temp19 * m10) - (temp18 * m10 + temp21 * m14 + temp13 * m2));
    out[14] = d * ((temp18 * m6 + temp23 * m14 + temp15 * m2) - (temp22 * m14 + temp14 * m2 + temp19 * m6));
    out[15] = d * ((temp22 * m10 + temp16 * m2 + temp21 * m6) - (temp20 * m6 + temp23 * m10 + temp17 * m2));

    return out;
};