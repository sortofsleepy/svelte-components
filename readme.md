Components
===

A series of potentially useful components for a website. 

Built with [Svelte](https://svelte.dev/)

Current Components
===
* An infinite scroller. 
  * Takes the content you pass to the component and will attempt to infinitely cycle them as you scroll.
  * Currently, only take into account of one scroll direction.
* An animated text element
  * Takes the text property you pass into it, splits it into separate chars and allows you to stagger animate each char
  * Currently, animates from the bottom and top.

Notes
===
* `npm install`
* `npm start` will start the dev server
* Components are designed to be as simple and straightforward as possible, so things may not reflect best practices.
* With the exception of the infinite scroller(for the moment), goal is to have dependencies all within the component.

Building things as a WebComponent
===
* You can build things in a WebComponent like structure. 
* Run `node compile.js` to build a component. Takes the argument `component` with the name of the component you want to compile. The script will search
`./src/components` for a name. 
```bash 

# note that you don't need the .svelte file extension
node compile.js --component=<component name> 

```
* See `compile.js` for a list of other possible arguments.

Building things as a library
===
* You can also build this as a library / package 
* This can be done using the command `npm run build`
* Note that currently this is experimental and there are some issues 
  * One issue currently is that building as a library will not work if the component contains a module that does not have a default export.
  * One other problem is that use of modules is not tested at the moment but in all likelyhood, the api would end up being something like 
  ```javascript
    import {Component} from "SuperComponents"
    
    let comp = new Component({
        target:document.body
    })```


Other ways 
===
You can always just copy / paste the component into your own project, assuming you're using Svelte of course.