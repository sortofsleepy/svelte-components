export default {

    env:{
        COMPILE_LIBRARY:process.env.COMPILE_LIBRARY !== undefined ? "LIBRARY" : false
    },
    mount:{

        public:"/",
        src:"/src"
    },
    plugins:[
        "@snowpack/plugin-svelte",
        ["@snowpack/plugin-typescript",{args:"-p ."}],
        ["@snowpack/plugin-sass",{

        compilerOptions:{
            loadPath:"./src/css"
        }
        }]
    ]
}

/*
   experiments:{

        "optimize":{
            bundle:true,
            minify:true,
            target:"es2018"
        }
    },
 */