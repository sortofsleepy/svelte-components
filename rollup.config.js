/**
 * Snowpack includes Rollup for module resolution so take advantage of that when building a library.
*/
import nodeResolve from "@rollup/plugin-node-resolve";
const typescript = require("@rollup/plugin-typescript");
const svelte = require("rollup-plugin-svelte");


export default {
    input:"./src/index.ts",
    output:{
        dir:"./library",
        exports:"default",
        sourcemap:true,
        format:"cjs"
    },

    plugins:[
        nodeResolve(),
        svelte({
            emitCss:false
        }),
        typescript({
            sourceMap:true
        })

    ]
}
